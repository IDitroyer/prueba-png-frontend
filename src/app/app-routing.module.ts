import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SharedModule } from 'src/app/shared.module'
import { LayoutsModule } from 'src/app/layouts/layouts.module'
import { AppPreloader } from 'src/app/app-routing-loader'
import { AuthGuard } from '@authGuard'

// layouts & notfound
import { LayoutAuthComponent } from 'src/app/layouts/Auth/auth.component'
import { LayoutMainComponent } from 'src/app/layouts/Main/main.component'
import { isAuthenticatedGuard } from '@AuthenticatedGuard'
import { AdminGuard } from './components/cleanui/system/Guard/admin.guard'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'escritorio',
    pathMatch: 'full',
  },
  {
    path: '',
    component: LayoutMainComponent,
    children: [
      {
        path: '',
        canActivate: [AdminGuard],
        loadChildren: () =>
          import('src/app/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
      },

      {
        path: 'configuracion',
        canActivate: [AdminGuard],
        loadChildren: () =>
          import('src/app/pages/configuracion/configuracion.module').then(m => m.ConfiguracionModule),
      },

      {
        path: '',
        canActivate: [AdminGuard],
        loadChildren: () =>
          import('src/app/pages/usuarios/usuarios.module').then(m => m.UsuariosModule),
      },
     
      {
        path: '',
        canActivate: [],
        loadChildren: () =>
          import('src/app/pages/mis-pagos/mis-pagos.module').then(m => m.MisPagosModule),
      },



      
    

    ],
  },
  {
    path: 'auth',
    canActivate: [isAuthenticatedGuard],
    component: LayoutAuthComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('src/app/pages/auth/auth.module').then(m => m.AuthModule),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '/auth/404',
  },
]

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes, {
      useHash: true,
      preloadingStrategy: AppPreloader,
    }),
    LayoutsModule,
  ],
  providers: [AppPreloader],
  exports: [RouterModule],
})
export class AppRoutingModule {}
