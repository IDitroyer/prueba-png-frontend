import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TruncatePipe } from './truncate.pipe';

import { TruncateFilePipe } from './truncate-file.pipe';
import { UniqueSearchPipe } from './unique-search.pipe';
import { searchPipe } from './search.pipe';
import { ShowErrorsDirective } from './showErrors.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
      searchPipe,
      TruncatePipe,
      UniqueSearchPipe,
      TruncateFilePipe,
      ShowErrorsDirective,

    ],
    exports: [
        searchPipe,
        TruncateFilePipe,
        UniqueSearchPipe,
        TruncatePipe,
        ShowErrorsDirective,
    ]
})
export class PipesModule { }
