import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { MisPagosRouterModule } from './mis-pagos-routing.module'
import { WidgetsComponentsModule } from 'src/app/components/kit/widgets/widgets-components.module'
import { FormsModule } from '@angular/forms'
import { ChartistModule } from 'ng-chartist'
import { NgApexchartsModule } from 'ng-apexcharts'
import { MisPagosComponent } from './mis-pagos/mis-pagos.component'



const COMPONENTS = [
  MisPagosComponent,


]

@NgModule({
  imports: [
    SharedModule,
    MisPagosRouterModule,
    WidgetsComponentsModule,
    FormsModule,
    ChartistModule,
    NgApexchartsModule,
  ],
  declarations: [...COMPONENTS],
  entryComponents: [

  ]
})
export class MisPagosModule {}
