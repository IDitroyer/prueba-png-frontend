import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NzModalService } from 'ng-zorro-antd';



@Component({
  selector: 'app-mis-pagos',
  templateUrl: './mis-pagos.component.html',
  styleUrls: ['./mis-pagos.component.scss']
})
export class MisPagosComponent implements OnInit {

  public form: FormGroup;
  public cargando: boolean;

  public isEdit: boolean;
  public grupo: any;
  public usuario: any;

  public pagoSearch: any;

  public cargandoTable: boolean;
  public pagos: any;

  itemSelected: any;

  nombreEntidad: string;
  listOfData: any[] = []
  bordered = false
  loading = false
  sizeChanger = false
  pagination = true
  header = true
  title = true
  footer = true
  fixHeader = false
  size = 'small'
  expandable = false
  checkbox = true
  allChecked = false
  indeterminate = false
  displayData: any[] = []
  simple = false
  noResult = false
  position = 'bottom'
  isVisible = true
  

  constructor(private auth: AuthService, private modalService: NzModalService, private service: ApiService, public dialog: MatDialog,
     private fb: FormBuilder) {
     
    }


    checkAll(value: boolean): void {
      this.displayData.forEach(data => {
        if (!data.disabled) {
          data.checked = value
        }
      })
      this.refreshStatus(null)
    }


    currentPageDataChange(
      $event: Array<any>,
    ): void {
      this.displayData = $event
      this.refreshStatus(null)
    }
  
    refreshStatus(data): void {
  
  
      if(data) {
        this.displayData.forEach(d => {
          d.checked = false;
        })
  
        if (this.itemSelected && data.id === this.itemSelected.id) {
          this.displayData.filter(value => value.id === data.id)[0].checked = false;
          this.itemSelected = null
        } else {
          this.displayData.filter(value => value.id === data.id)[0].checked = true;
          this.itemSelected = this.displayData.filter(value => value.id === data.id)[0];
        }  
      } else {
        this.itemSelected = null;
      }

    }
    


    cambiarEstado() {
      this.service.post(`pagos/save`, this.itemSelected).subscribe(
        res => {},
        err => {this.itemSelected.activo = !this.itemSelected.activo}
      )
    }


    eliminarPago(id) {

      
  this.modalService.confirm({
    nzTitle: `¿Desea eliminar el pago actual?`,
    nzOkText: 'Si',
    nzOkType: 'primary',
    nzOnOk: () => {
     
      
      this.service.delete(`pagos/eliminar/${id}`).subscribe(
        res => {this.getPagos();},
        err => {}
      )
      
    },
    nzCancelText: 'No',
    nzOnCancel: () => console.log('Cancel'),
  });


    }


    descargarSoporte(filename) {
      window.open(`http://localhost:3000/pagos/ver/${filename}`, '_blank');
      
    }


  


    async getPagos() {
      this.cargandoTable = true;
      this.pagos = await this.service.get(`pagos/usuario`);
      this.cargandoTable = false;
      this.listOfData = this.pagos;
      console.log(this.pagos)
    }



  ngOnInit(): void {

    this.getPagos();

  }


}

