import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'
import { MisPagosComponent } from './mis-pagos/mis-pagos.component'


const routes: Routes = [
  {
    path: 'mis-pagos',
    component:  MisPagosComponent,
    data: { title: 'MisPagos' },
  },

]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  providers: [],
  exports: [RouterModule],
})
export class MisPagosRouterModule {}
