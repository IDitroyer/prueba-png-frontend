import { Component, OnInit } from '@angular/core'
import { ApiService } from '@service'
import { NzNotificationService } from 'ng-zorro-antd/notification'

declare var require: any
const data: any = require('./data.json')

@Component({
  selector: 'app-dashboard-alpha',
  templateUrl: './alpha.component.html',
})
export class DashboardAlphaComponent implements OnInit {
  chartCardData = data.chartCardData
  chartCardGraphOptions: object
  paymentAccountsData = data.paymentAccountsData
  paymentCardsData = data.paymentCardsData
  paymentTransactionsData = data.paymentTransactionsData
  pricingItemData = data.pricingItemData
  referalsData = data.referalsData
  displayReferalsData = [...this.referalsData]
  sortNameReferals = null
  sortValueReferals = null

  estadisticas: any;

  public barData;

  public pieData;
  pieOptions = {}

 
  barOptions = {
    scales: {
      xAxes: [
        {
          stacked: true,
        },
      ],
      yAxes: [
        {
          stacked: true,
        },
      ],
    },
  }

  constructor(private notification: NzNotificationService,
    private service: ApiService,) {
    this.chartCardGraphOptions = {
      options: {
        axisX: {
          showLabel: false,
          showGrid: false,
          offset: 0,
        },
        axisY: {
          showLabel: false,
          showGrid: false,
          offset: 0,
        },
        showArea: true,
        high: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
        },
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
        },
        fullWidth: true,
        height: '110px',
        showPoint: false,
      },
      low: 20,
      type: 'Line',
    }
  }
  ngOnInit() {
//    this.getEstadisticas();
  }

  async getOtEstadisticas() {
    let d = await this.service.get(`cronograma/estadisticas/ot`);
    this.pieData = {
      labels: ['creada', 'cotizacion', 'asignada', 'ejecutada', 'mirada', 'cancelada'],
      datasets: [
        {
          data: [],
          backgroundColor: ['#657484', '#7d8863', '#4b7cf3', '#41b883', '#5c7d6f', '#7d5c5c'],
          hoverBackgroundColor: ['#657484', '##7d8863', '#4b7cf3', '#41b883', '#5c7d6f', '#7d5c5c'],
        },
      ],
    }

    this.pieData.datasets[0].data.push(d.creada);
    this.pieData.datasets[0].data.push(d.cotizacion);
    this.pieData.datasets[0].data.push(d.asignada);
    this.pieData.datasets[0].data.push(d.ejecutada);
    this.pieData.datasets[0].data.push(d.mirada);
    this.pieData.datasets[0].data.push(d.cancelada);


  }

 
  async getMantenimientoEstadisticas() {
    let d = await this.service.get(`calendario/mantenimientos/estadisticas`);

    this.barData = {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      datasets: [
        {
          label: 'Mantenimientos programados',
          backgroundColor: [
          ],
          borderColor: [
          ],
          borderWidth: 1,
          data: [],
        },
      ],
    }

    for (let i = 0; i < d.length; i++) {
      const da = d[i];
      this.barData.datasets[0].backgroundColor.push('#4b7cf3');
      this.barData.datasets[0].borderColor.push('#4b7cf3');
      this.barData.datasets[0].data.push(da.mantenimientos);
      
    }


    this.getOtEstadisticas();


  }


  async getEstadisticas() {
    this.estadisticas = await this.service.get(`estadisticas`);
    this.getMantenimientoEstadisticas();
    console.log(this.estadisticas)
  }

  sort(sort: { key: string; value: string }): void {
    this.sortNameReferals = sort.key
    this.sortValueReferals = sort.value
    this.search()
  }

  search(): void {
    if (this.sortNameReferals && this.sortValueReferals) {
      this.displayReferalsData = this.referalsData.sort((a, b) =>
        this.sortValueReferals === 'ascend'
          ? a[this.sortNameReferals] > b[this.sortNameReferals]
            ? 1
            : -1
          : b[this.sortNameReferals] > a[this.sortNameReferals]
          ? 1
          : -1,
      )
    } else {
      this.displayReferalsData = this.referalsData
    }
  }
}
