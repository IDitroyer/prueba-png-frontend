import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { ConfiguracionRouterModule } from './configuracion-routing.module'
import { WidgetsComponentsModule } from 'src/app/components/kit/widgets/widgets-components.module'
import { FormsModule } from '@angular/forms'
import { ChartistModule } from 'ng-chartist'
import { NgApexchartsModule } from 'ng-apexcharts'
import { ParametrosComponent } from './parametros/parametros.component'
import { ParametrosFormComponent } from './parametros/parametros-form/parametros-form.component'
import { GruposFormComponent } from './parametros/grupos-form/grupos-form.component';

const COMPONENTS = [
  ParametrosComponent,
  ParametrosFormComponent,
  GruposFormComponent,

]

@NgModule({
  imports: [
    SharedModule,
    ConfiguracionRouterModule,
    WidgetsComponentsModule,
    FormsModule,
    ChartistModule,
    NgApexchartsModule,
  ],
  declarations: [...COMPONENTS],
  entryComponents: [

  ]
})
export class ConfiguracionModule {}
