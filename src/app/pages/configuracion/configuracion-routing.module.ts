import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'
import { ParametrosComponent } from './parametros/parametros.component'

const routes: Routes = [
  {
    path: 'parametros',
    component: ParametrosComponent,
    data: { title: 'Parametros' },
  },
]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  providers: [],
  exports: [RouterModule],
})
export class ConfiguracionRouterModule {}
