import { Component, OnInit } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
const data: any = require('./data.json')
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ParametrosFormComponent } from './parametros-form/parametros-form.component';
import { NzModalService } from 'ng-zorro-antd/modal'
import { GruposFormComponent } from './grupos-form/grupos-form.component';


@Component({
  selector: 'app-parametros',
  templateUrl: './parametros.component.html',
  styleUrls: ['./parametros.component.scss']
})
export class ParametrosComponent implements OnInit {
  cargando: boolean;
  cargandoTable: boolean;

  parametros: any;
  parametrosSearch: any;
  itemSelected: any;

  grupos: any;
  grupo: any;
  gruposSearch: string;



  nombreEntidad: string;
  listOfData: any[] = []
  bordered = false
  loading = false
  sizeChanger = false
  pagination = true
  header = true
  title = true
  footer = true
  fixHeader = false
  size = 'small'
  expandable = false
  checkbox = true
  allChecked = false
  indeterminate = false
  displayData: any[] = []
  simple = false
  noResult = false
  position = 'bottom'
  isVisible = true

  cargandoEstado: boolean;
  cargandoEliminar: boolean;

  

  constructor(private auth: AuthService, private service: ApiService, public dialog: MatDialog, private modalService: NzModalService) {
    this.cargando = false;
    this.cargandoTable = false;
    this.cargandoEstado = false;
    this.cargandoEliminar = false;
    this.gruposSearch = '';
    this.parametrosSearch = '';
  }





  currentPageDataChange(
    $event: Array<any>,
  ): void {
    this.displayData = $event
    this.refreshStatus(null)
  }

  refreshStatus(data): void {


    if(data) {
      this.displayData.forEach(d => {
        d.checked = false;
      })

      if (this.itemSelected && data.id === this.itemSelected.id) {
        this.displayData.filter(value => value.id === data.id)[0].checked = false;
        this.itemSelected = null
      } else {
        this.displayData.filter(value => value.id === data.id)[0].checked = true;
        this.itemSelected = this.displayData.filter(value => value.id === data.id)[0];
      }  
    } else {
      this.itemSelected = null;
    }
/* 
    const validData = this.displayData.filter(value => !value.disabled)
    const allChecked = validData.length > 0 && validData.every(value => value.checked === true)
    const allUnChecked = validData.every(value => !value.checked)
    this.allChecked = allChecked
    this.indeterminate = !allChecked && !allUnChecked */
  }

  checkAll(value: boolean): void {
    this.displayData.forEach(data => {
      if (!data.disabled) {
        data.checked = value
      }
    })
    this.refreshStatus(null)
  }

  ngOnInit(): void {

    this.getGrupos();
  }


  gruposForm(tipo: number ): void {
    const dialogRef = this.dialog.open(GruposFormComponent, {
      width: '40%',
      data: {tipo: tipo, grupo: this.grupo}
    });
    dialogRef.afterClosed().subscribe(res => {
      switch (res) {
        case 1:
          this.grupo = null;
          this.getGrupos();
          break;3
      }
    });
  }


  parametrosForm(tipo: number ): void {
    const dialogRef = this.dialog.open(ParametrosFormComponent, {
      width: '55%',
      data: {tipo: tipo, parametro: this.itemSelected, grupo: this.grupo}
    });
    dialogRef.afterClosed().subscribe(res => {
      switch (res.tipo) {
        case 1:
          this.getParametros();
          break;
      }
    });
  }

  cambiarEstado() {
    this.cargandoEstado = true;
    console.log(this.itemSelected);
    this.service.post(`parametros/save`, this.itemSelected).subscribe(
      res => {this.cargandoEstado = false;},
      err => {this.cargandoEstado = false; this.itemSelected.estado = !this.itemSelected.estado}
    )
  }



  eliminarParametro() {

    this.modalService.confirm({
      nzTitle: `¿Desea eliminar el parametro?`,
      nzContent: `<b style="color: red;">${this.itemSelected.nombre}</b>`,
      nzOkText: 'Si',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.cargandoEliminar = true;
        this.service.delete(`parametros/delete/${this.itemSelected.id}`).subscribe(
          res => {this.cargandoEliminar = false; this.getParametros()},
          err => {this.cargandoEliminar = false;}
        )
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });

  }


  eliminarGrupo() {

    this.modalService.confirm({
      nzTitle: `¿Desea eliminar el grupo?`,
      nzContent: `<b style="color: red;">${this.grupo.nombre} </b> : <span> ${this.grupo.descripcion}<span>`,
      nzOkText: 'Si',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.cargandoEliminar = true;
        this.service.delete(`grupos/delete/${this.grupo.id}`).subscribe(
          res => {this.cargandoEliminar = false; this.getGrupos()},
          err => {this.cargandoEliminar = false;}
        )
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });

  }





  checkItem(data) : boolean {
    return true;
  }

  setGrupo(grupo) {
    this.grupo = grupo;
    this.getParametros();
  }

  async getParametros() {
    this.cargandoTable = true;
    this.parametros = await this.service.get(`parametros/grupo/${this.grupo.id}`);
    this.cargandoTable = false;
    this.listOfData = this.parametros;
    console.log(this.parametros);
  }

  async getGrupos() {
    this.cargando = true;
    this.grupos = await this.service.get('grupos/listwp');
    this.cargando = false;
    console.log(this.grupos)
  }


  noResultChange(status: boolean): void {
    this.listOfData = []
    if (!status) {
      this.ngOnInit()
    }
  }
}

