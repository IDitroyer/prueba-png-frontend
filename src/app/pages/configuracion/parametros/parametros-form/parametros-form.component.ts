import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'



@Component({
  selector: 'app-parametros-form',
  templateUrl: './parametros-form.component.html',
  styleUrls: ['./parametros-form.component.scss']
})
export class ParametrosFormComponent implements OnInit {

  public form: FormGroup;
  public cargando: boolean;

  public isEdit: boolean;
  public grupo: any;
  public parametro: any;
  

  constructor(private auth: AuthService, private service: ApiService, public dialog: MatDialog,
    public dialogRef: MatDialogRef<ParametrosFormComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.data.tipo === 1 ? this.isEdit = true : this.isEdit = false;
      this.grupo = this.data.grupo
      this.parametro = this.data.parametro; 
      this.cargando = false;

      this.form = fb.group({
        codigo: ['', [Validators.required]],
        valor: [],
        fecha: [],
        nombre: ['', [Validators.required]],
        nombre_corto: ['', [Validators.required]],
      })

      this.parametro ? this.form.patchValue({...this.parametro}) : null;

    }


    guardar() {
      if (this.form.valid) {
        let body = {...this.form.value};
        body.id_grupo = this.grupo.id;

        if (this.isEdit) {
          body.id = this.parametro.id;
          body.id_grupo = this.parametro.id_grupo;
          body.estado = this.parametro.estado;
        }

        console.log(body);

        this.cargando = true;
        this.service.post(`parametros/save`, body).subscribe(
          res => {this.cargando = false, this.onNoClick(1, res);},
          err => {this.cargando = false}
        )

      }
    }

    onNoClick(tipo: number, parametro : any): void {
      this.dialogRef.close({tipo : tipo, parametro : parametro});
    }
  ngOnInit(): void {


  }


}

