import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'



@Component({
  selector: 'app-grupos-form',
  templateUrl: './grupos-form.component.html',
  styleUrls: ['./grupos-form.component.scss']
})
export class GruposFormComponent implements OnInit {

  public form: FormGroup;
  public cargando: boolean;

  public isEdit: boolean;
  public grupo: any;
  

  constructor(private auth: AuthService, private service: ApiService, public dialog: MatDialog,
    public dialogRef: MatDialogRef<GruposFormComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.data.tipo === 1 ? this.isEdit = true : this.isEdit = false;
      this.grupo = this.data.grupo
      this.cargando = false;

      this.form = fb.group({
        nombre: ['', [Validators.required]],
        descripcion: [''],
      })

      this.isEdit ? this.form.patchValue({...this.grupo}) : null;

    }


    guardar() {
      if (this.form.valid) {
        let body = {...this.form.value};
        if (this.isEdit) {
          body.id = this.grupo.id;
        }

        console.log(body);

        this.cargando = true;
        this.service.post(`grupos/save`, body).subscribe(
          res => {this.cargando = false, this.onNoClick(1);},
          err => {this.cargando = false}
        )

      }
    }

  onNoClick(tipo: number): void {
    this.dialogRef.close(tipo);
  }
  ngOnInit(): void {


  }


}

