import { Component, OnInit } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
const data: any = require('./data.json')
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { UsuariosFormComponent } from './usuarios-form/usuarios-form.component';
import { NzModalService } from 'ng-zorro-antd/modal'
import { PagosFormComponent } from './pagos-form/pagos-form.component';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  cargando: boolean;
  cargandoTable: boolean;

  usuarios: any;
  usuariosSearch: any;
  itemSelected: any;

  grupos: any;
  grupo: any;
  gruposSearch: string;



  nombreEntidad: string;
  listOfData: any[] = []
  bordered = false
  loading = false
  sizeChanger = false
  pagination = true
  header = true
  title = true
  footer = true
  fixHeader = false
  size = 'small'
  expandable = false
  checkbox = true
  allChecked = false
  indeterminate = false
  displayData: any[] = []
  simple = false
  noResult = false
  position = 'bottom'
  isVisible = true

  cargandoEstado: boolean;
  cargandoEliminar: boolean;

  

  constructor(private auth: AuthService, private service: ApiService, public dialog: MatDialog, private modalService: NzModalService) {
    this.cargando = false;
    this.cargandoTable = false;
    this.cargandoEstado = false;
    this.cargandoEliminar = false;
    this.gruposSearch = '';
    this.usuariosSearch = '';
  }



  verPagos() {

    const dialogRef = this.dialog.open(PagosFormComponent, {
      width: '60%',
      data: {usuario: this.itemSelected}
    });
    dialogRef.afterClosed().subscribe(res => {
      switch (res) {
        case 1:
          this.getUsuarios();
          break;
      }
    });

  }



  currentPageDataChange(
    $event: Array<any>,
  ): void {
    this.displayData = $event
    this.refreshStatus(null)
  }

  refreshStatus(data): void {


    if(data) {
      this.displayData.forEach(d => {
        d.checked = false;
      })

      if (this.itemSelected && data.id === this.itemSelected.id) {
        this.displayData.filter(value => value.id === data.id)[0].checked = false;
        this.itemSelected = null
      } else {
        this.displayData.filter(value => value.id === data.id)[0].checked = true;
        this.itemSelected = this.displayData.filter(value => value.id === data.id)[0];
      }  
    } else {
      this.itemSelected = null;
    }

  }

  checkAll(value: boolean): void {
    this.displayData.forEach(data => {
      if (!data.disabled) {
        data.checked = value
      }
    })
    this.refreshStatus(null)
  }

  ngOnInit(): void {

    this.getUsuarios();
  }





  usuariosForm(tipo: number ): void {
    const dialogRef = this.dialog.open(UsuariosFormComponent, {
      width: '55%',
      data: {tipo: tipo, usuario: this.itemSelected}
    });
    dialogRef.afterClosed().subscribe(res => {
      switch (res.tipo) {
        case 1:
          this.getUsuarios();
          break;
      }
    });
  }

  cambiarEstado() {
    this.cargandoEstado = true;
    console.log(this.itemSelected);
    this.service.post(`users/custom/save`, this.itemSelected).subscribe(
      res => {this.cargandoEstado = false;},
      err => {this.cargandoEstado = false; this.itemSelected.activo = !this.itemSelected.activo}
    )
  }



  eliminarUsuario() {

    this.modalService.confirm({
      nzTitle: `¿Desea eliminar el usuario?`,
      nzContent: `<b style="color: red;">${this.itemSelected.nombre}</b>`,
      nzOkText: 'Si',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.cargandoEliminar = true;
        this.service.delete(`users/delete/${this.itemSelected.id}`).subscribe(
          res => {this.cargandoEliminar = false; this.getUsuarios()},
          err => {this.cargandoEliminar = false;}
        )
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });

  }


/*   eliminarGrupo() {

    this.modalService.confirm({
      nzTitle: `¿Desea eliminar el grupo?`,
      nzContent: `<b style="color: red;">${this.grupo.nombre} </b> : <span> ${this.grupo.descripcion}<span>`,
      nzOkText: 'Si',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.cargandoEliminar = true;
        this.service.delete(`grupos/delete/${this.grupo.id}`).subscribe(
          res => {this.cargandoEliminar = false; this.getGrupos()},
          err => {this.cargandoEliminar = false;}
        )
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });

  } */









  async getUsuarios() {
    this.cargandoTable = true;
    this.usuarios = await this.service.get('users/list');
    this.cargandoTable = false;
    this.listOfData = this.usuarios;
    console.log(this.usuarios)
  }


  noResultChange(status: boolean): void {
    this.listOfData = []
    if (!status) {
      this.ngOnInit()
    }
  }
}

