import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '@authService';
import { ApiService } from '@service';
declare var require: any
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { emailValidator, matchingPasswords, numberLimits } from 'src/app/components/validators/app-validators';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';



@Component({
  selector: 'app-agregar-pago',
  templateUrl: './agregar-pago.component.html',
  styleUrls: ['./agregar-pago.component.scss']
})
export class AgregarPagoComponent implements OnInit {

  public form: FormGroup;
  public cargando: boolean;

  public isEdit: boolean;
  public pago: any;
  public usuario: any;
  public tipos: any;



  constructor(private auth: AuthService, private notification: NzNotificationService, private modalService: NzModalService, private service: ApiService, public dialog: MatDialog,
    public dialogRef: MatDialogRef<AgregarPagoComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.data.tipo === 1 ? this.isEdit = true : this.isEdit = false;
    this.pago = this.data.pago;
    this.usuario = this.data.usuario;
    this.cargando = false;


    this.form = fb.group({
      fecha: ['', [Validators.required]],
      tipo: ['', [Validators.required]],
      referencia: ['', [numberLimits({ min: 6 })]],
    })


    if (this.pago != null) {
      this.form.patchValue({ ...this.pago })
    }
  }



  save() {

    if (this.form.valid) {

      this.modalService.confirm({
        nzTitle: `¿Desea guardar los cambios del pago actual?`,
        nzOkText: 'Si',
        nzOkType: 'primary',
        nzOnOk: () => {
          if (this.isEdit) {
            this.editar();
          } else {
            this.crear();
          }
        },
        nzCancelText: 'No',
        nzOnCancel: () => console.log('Cancel'),
      });

    } else {
      this.notification.warning('Revise el formulario', 'El formulario contiene errores');
    }
  }



  crear() {
    let body = { ...this.form.value };
    body.id_usuario = this.usuario.id;
    this.service.post(`pagos/crear`, body).subscribe(
      res => { this.cargando = false, this.onNoClick(1, res); },
      err => { this.cargando = false }
    )
  }

  editar() {
    let body = { ...this.form.value };
    body.id = this.usuario.id;
    body.password = this.usuario.password;
    body.activo = this.usuario.activo;
    this.service.post(`usuarios/custom/save`, body).subscribe(
      res => { this.cargando = false, this.onNoClick(1, res); },
      err => { this.cargando = false }
    )
  }



  async getTipos() {
    this.tipos = await this.service.get('parametros/grupo/activos/4');
  }


  onNoClick(tipo: number, parametro: any): void {
    this.dialogRef.close({ tipo: tipo, parametro: parametro });
  }


  ngOnInit(): void {
    this.getTipos();
  }


}

