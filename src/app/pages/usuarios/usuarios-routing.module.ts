import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'
import { UsuariosComponent } from './usuarios/usuarios.component'


const routes: Routes = [
  {
    path: 'usuarios',
    component:  UsuariosComponent,
    data: { title: 'Usuarios' },
  },

]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  providers: [],
  exports: [RouterModule],
})
export class UsuariosRouterModule {}
