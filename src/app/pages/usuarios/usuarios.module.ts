import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { UsuariosRouterModule } from './usuarios-routing.module'
import { WidgetsComponentsModule } from 'src/app/components/kit/widgets/widgets-components.module'
import { FormsModule } from '@angular/forms'
import { ChartistModule } from 'ng-chartist'
import { NgApexchartsModule } from 'ng-apexcharts'
import { UsuariosComponent } from './usuarios/usuarios.component'
import { UsuariosFormComponent } from './usuarios/usuarios-form/usuarios-form.component'
import { PagosFormComponent } from './usuarios/pagos-form/pagos-form.component'
import { AgregarPagoComponent } from './usuarios/pagos-form/agregar-pago/agregar-pago.component'


const COMPONENTS = [
  UsuariosComponent,
  UsuariosFormComponent,
  PagosFormComponent,
  AgregarPagoComponent,

]

@NgModule({
  imports: [
    SharedModule,
    UsuariosRouterModule,
    WidgetsComponentsModule,
    FormsModule,
    ChartistModule,
    NgApexchartsModule,
  ],
  declarations: [...COMPONENTS],
  entryComponents: [

  ]
})
export class UsuariosModule {}
