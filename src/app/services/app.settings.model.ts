export class Settings {
  constructor(
    public loadingtext: string,
    public navbartext: string,
    public endpoints: any,
    public messages: Array<any>,
    public tipoAuth: number,
  ) {}
}
