
export const getMenuDataAdmin: any[] = [

  {
    category: true,
    title: 'Administrador',
  },
  {
    title: 'Escritorio',
    key: 'escritorio',
    icon: 'fe fe-clipboard',
    url: '/escritorio',
  },
  {
    title: 'Configuración',
    key: 'configuracion',
    icon: 'fe fe-settings',
    count: 14,
    children: [

      {
        title: 'Parámetros',
        key: 'parametros',
        url: '/configuracion/parametros',
      }, 

    ],
  },
  {
    title: 'Usuarios y pagos',
    key: 'usuarios',
    icon: 'fe fe-users',
    url: '/usuarios',
  },

]



export const getMenuDataUser: any[] = [
  {
    category: true,
    title: 'Miembro usuario',
  },
  {
    title: 'Mis pagos',
    key: 'mis-pagos',
    icon: 'fe fe-package',
    url: '/mis-pagos',
  },

]
