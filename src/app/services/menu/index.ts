import { Injectable } from '@angular/core'
import { AuthService } from '@authService'
import { Observable, of } from 'rxjs'
import { getMenuDataAdmin } from './config'
import { getMenuDataUser } from './config'

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  constructor(private auth: AuthService) {}

  getMenuData(): Observable<any[]> {

    const rol = this.auth.obtenerDatosToken(localStorage.getItem('token')).rol;
    console.log('ROL', rol);
    if (rol) {
      switch (rol.nombre) {
        case 'ADMIN':
          return of(getMenuDataAdmin);
        default:
          return of(getMenuDataUser);
      }
    }
    return of(getMenuDataUser);
  }
}
