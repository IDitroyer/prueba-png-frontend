import {
HttpInterceptor,
HttpHandler,
HttpRequest,
HttpEvent,
HttpResponse,
HttpErrorResponse,
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { tap, catchError } from 'rxjs/operators'
import { NzNotificationService } from 'ng-zorro-antd'
import { Console } from 'console'

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
constructor(private notification: NzNotificationService) {}

public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  

    return next.handle(req).pipe(
    tap(evt => {
        if (evt instanceof HttpResponse) {
        
                             if (evt.body.message) {
                              
                               this.notification.success('Correcto', evt.body.message);
                            } 
          //close spinner
        }
    }),
    catchError((error: any) => {
        if (error.error instanceof ErrorEvent) {
        this.notification.error('Error al consultar', error.error.message)
        } else {
        switch (error.status) {
            case 400:
            this.notification.error('Solicitud incorrecta!', error.error.message)
            break
            case 401: 
            this.notification.error('No autorizado!', error.error.message)
            break
            case 403:
            this.notification.error('Prohibido!', error.error.message)
            break
            case 404:
            this.notification.error('Recurso no encontrado!', error.error.message)
            break
            case 405:
            this.notification.error('No permitido!', error.error.message)
            break
            case 408:
            this.notification.error('Se agoto el tiempo de espera!', error.error.message)
            break
            case 415:
            console.log(error)
            this.notification.error('Formato no soportado!', error.error.message)
            break
            case 500:
            console.log(error)

            this.notification.error('Error en el servidor!', error.error.message)
            break
            case 501:
            this.notification.error('Consulta no soportada!', error.error.message)
            break
        }
        }
        return of(error)
    }),
    )
}
}
